#pragma once
// Arkadia
#include "ark_core/ark_core.hpp"

namespace Tray {

// Used to get events from the Windows Loop that represents the Tray Icon.
constexpr u32 WM_TRAY = (WM_USER + 1);

//
using TrayEvent_t = ark::Gfx::Win32::WindowUnderlyingSystemEvent;

HICON LoadIcon(ark::String const &filename);
void  DrawIcon(HICON const icon_handle, HWND const window_handle);
void  HideIcon();

void HandleTrayEvents(TrayEvent_t const &event);

} // namespace Tray
