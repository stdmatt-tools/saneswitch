#pragma once
// Arkadia
#include <ark_core/ark_core.hpp>

namespace Keyboard {

typedef void(*ChangeState_t)(ark::KeyboardEvent const &);


HHOOK RegisterHandler  (ChangeState_t const callback_func);
void  UnregisterHandler(HHOOK         const handle);

} // namespace Keyboard
