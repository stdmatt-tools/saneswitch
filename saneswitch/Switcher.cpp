// Header
#include "Switcher.hpp"

//
// Windows Callback Functions...
//
//------------------------------------------------------------------------------
BOOL CALLBACK
_FindMatchingWindowHandlesProc(HWND const window_handle, LPARAM const l_param)
{
    if(!IsWindowVisible(window_handle)) {
        return true;
    }

    Switcher::SwitchData *data = Cast<Switcher::SwitchData*>(l_param);
    ARK_ASSERT_NOT_NULL(data);

    // Window Text
    u32 const window_text_len = GetWindowTextLength(window_handle);
    if(window_text_len == 0) {
        return true;
    }

    char window_text_buffer[1024] = {};
    GetWindowText(window_handle, window_text_buffer, window_text_len + 1);

    // Get process id and executable name
    DWORD process_id;
    GetWindowThreadProcessId(window_handle, &process_id);
    ark::String const exe_name = ark::Win32::FindExecutableNameFromProcessId(process_id);

    // Check if this window handle is from the program that we are looking for.
    if(exe_name == data->target_exe_name) {
        // ark::PrintLn("{} _ {}", exe_name, data->target_exe_name);
        data->matching_window_handles.PushBack(window_handle);
    }

    return TRUE;
}

//
// App Switch Functions
//
//------------------------------------------------------------------------------
void
Switcher::CreateData(SwitchData *data)
{
    // Already created...
    if(data->has_data) {
        return;
    }

    data->has_data = true;

    //
    // Grab the info about the current Foreground Window.
    // All the switching will be based upon this application.
    data->fg_window_handle = GetForegroundWindow();

    DWORD fg_process_id;
    GetWindowThreadProcessId(data->fg_window_handle, &fg_process_id);

    data->target_exe_name = ark::Win32::FindExecutableNameFromProcessId(fg_process_id);
    EnumWindows(_FindMatchingWindowHandlesProc, (LPARAM)(data));

    u32 const handles_count = Cast<u32>(data->matching_window_handles.Count());
    for(u32 i = 0; i < handles_count; ++i) {
        // @notice(stdmatt): We could set this on the FindMatchingWindowHandlesProc
        // but I think that doing there is not clear...
        //
        // I understand the design considerations of the win32 api back then
        // but this callback is just too messy. Moreover we gonna have
        // so small number of windows that this doesn't even matter.
        //
        // stdmatt - Feb 12, 2021

        // This is our window ;D
        if(data->matching_window_handles[i] == data->fg_window_handle) {
            data->current_index  = i;
            data->original_index = i;
        }
    }
}

//------------------------------------------------------------------------------
void
Switcher::DestroyData(SwitchData *data)
{
    if(!data->has_data) {
        return;
    }

    data->fg_window_handle = nullptr;
    data->current_index    = INVALID_INDEX;
    data->original_index   = INVALID_INDEX;

    data->matching_window_handles.Clear();
    data->target_exe_name        .Clear();

    data->has_data = false;
}


//------------------------------------------------------------------------------
void
Switcher::Switch(SwitchData *data, bool const backwards)
{
    i32 const count = Cast<u32>(data->matching_window_handles.Count());
    i32 const curr  = data->current_index;
    i32 const offset = (backwards) ? -1 : +1;
    i32 next = curr + offset;
    if(next < 0) {
        next = count -1;
    } else if(next >= count) {
        next = 0;
    }
    // ark::PrintLn("Count {} - Curr: {} - Next: {} - backwards: {}", count, curr, next, backwards);
    HWND const window_handle = data->matching_window_handles[next];
    SetForegroundWindow(window_handle);

    data->current_index = next;
}
